<!-- Directory Application
	 Author: Daniel dbm0100@yahoo.com
	 A basic directory application with
	 create, read, update, delete functionality
	 Version 0.1 completed 11-27.
	 Hope to revisit this and improve UI and
	 implement more AJAX!-->
<!DOCTYPE html>
<html lang = "en">
    <head>
        <meta charset="utf-8"/>
        <title>Daniel Meakin | Portfolio</title>
        <link rel = "stylesheet" href = "../../main.css"/>
        <link rel = "stylesheet" href = "style.css"/>
        <script language ="javascript" type ="text/javascript" src ="cashReg.js"></script>

    </head>
    <body>
        <div id = "big_wrapper">
            <header id = "top_header">
                <h1>Daniel Meakin | Portfolio</h1>            
            </header>
            <nav id = "top_menu">
                <ul>
                    <li><a href = "../../index.html">Home</a></li>
                    <li><a href = "../../resume.html">Resume</a></li>
                    <li><a href = "../../portfolio/">Portfolio</a></li>
                </ul>
            </nav>
            <section id = "section_main">
				<h2>Cash Register</h2>
				<table id ="registerOutput">
					<tr>
						<td>
							<input readonly id ="total"type ="text" value ="0.00"/>
						</td>
						<td rowspan ="2">
							<textarea readonly id ="log"></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<input id ="newEntry" type ="button" onclick ="newEntry();" value ="New Transaction">
						</td>
				</table>
				<table id ="itemsTable">
					<tr>
						<td>
							<input type="button" onclick ="itemSelect('butter');" value="butter"/>
						</td>
						<td>
							<input type ="button" onclick ="itemSelect('eggs');" value ="eggs"/>
						</td>
						<td>
							<input type ="button" onclick ="itemSelect('milk');" value ="milk"/>
						</td>
						<td>
							<input type ="button" onclick ="itemSelect('ice_cream');" value ="ice cream"/>
						</td>
						<td>
							<input type ="button" onclick ="itemSelect('cheese');" value ="cheese"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type ="button" onclick ="itemSelect('bread');" value ="bread"/>
						</td>
						<td>
							<input type ="button" onclick ="itemSelect('tortillas');" value ="tortillas"/>
						</td>
						<td>
							<input type ="button" onclick ="itemSelect('chips');" value ="chips"/>
						</td>
						<td>
							<input type ="button" onclick ="itemSelect('cereal');" value ="cereal"/>
						</td>
						<td>
							<input type ="button" onclick ="itemSelect('pop_tarts');" value ="pop tarts"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type ="button" onclick ="itemSelect('apple');" value ="apple"/>
						</td>
						<td>
							<input type ="button" onclick ="itemSelect('orange');"  value ="orange"/>
						</td>
						<td>
							<input type ="button" onclick ="itemSelect('grapes');"  value ="grapes"/>
						</td>
						<td>
							<input type ="button" onclick ="itemSelect('watermelon');"  value ="watermelon"/>
						</td>
						<td>
							<input type ="button" onclick ="itemSelect('pear');"  value ="pear"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type ="button" onclick ="itemSelect('carrot');"  value ="carrot"/>
						</td>
						<td>
							<input type ="button" onclick ="itemSelect('cucumber');"  value ="cucumber"/>
						</td>
						<td>
							<input type ="button" onclick ="itemSelect('corn');"  value ="corn"/>
						</td>
						<td>
							<input type ="button" onclick ="itemSelect('lettuce');"  value ="lettuce"/>
						</td>
						<td>
							<input type ="button" onclick ="itemSelect('tomato');"  value ="tomato"/>
						</td>
					</tr>
				</table>
				<table id ="calcTable">
					<tr>
						<td>
							<input type ="button" onclick ="quantitySelect(1);" value ="1"/>
						</td>
						<td>
							<input type ="button" onclick ="quantitySelect(2);" value ="2"/>
						</td>
						<td>
							<input type ="button" onclick ="quantitySelect(3);"value ="3"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type ="button" onclick ="quantitySelect(4);" value ="4"/>
						</td>
						<td>
							<input type ="button" onclick ="quantitySelect(5);" value ="5"/>
						</td>
						<td>
							<input type ="button" onclick ="quantitySelect(6);" value ="6"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type ="button" onclick ="quantitySelect(7);" value ="7"/>
						</td>
						<td>
							<input type ="button" onclick ="quantitySelect(8);" value ="8"/>
						</td>
						<td>
							<input type ="button" onclick ="quantitySelect(9);" value ="9"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type ="button" onclick ="clearLog();" value ="Clear"/>
						</td>
						<td>
							<input type ="button" onclick ="quantitySelect(0);" value ="0"/>
						</td>
						<td>
							<input type ="button" onclick ="runningTotal();"value ="Enter"/>
						</td>
					</tr>
				</table>
					
            </section>
        </div>
    </body>
</html>
