var cashRegister =
{
    total:0,
	quantityButton:"",
	productButton:"",
    //Dont forget to add your property
    add: function(itemCost)
	{
        this.total +=  itemCost;
        this.lastTransactionAmount = itemCost;
    },
    scan: function()
	{
        this.add(this.subtotal());
        return true;
    },
	subtotal: function ()
	{
		switch (this.productButton)
		{
			case "butter": return(0.86 * this.quantityButton); break;
			case "eggs": return(0.98 * this.quantityButton); break;
			case "milk": return(1.23 * this.quantityButton); break;
			case "ice_cream": return(2.99 * this.quantityButton); break;
			case "cheese": return(1.49 * this.quantityButton); break;
			case "bread": return(1.98 * this.quantityButton); break;
			case "tortillas": return(1.68 * this.quantityButton); break;
			case "chips": return(2.98 * this.quantityButton); break;
			case "cereal": return(3.49 * this.quantityButton); break;
			case "pop_tarts": return(2.55 * this.quantityButton); break;
			case "apple": return(.79 * this.quantityButton); break;
			case "orange": return(.89 * this.quantityButton); break;
			case "grapes": return(1.19 * this.quantityButton); break;
			case "watermelon": return(5.59 * this.quantityButton); break;
			case "pear": return(.87 * this.quantityButton); break;
			case "carrot": return(.39 * this.quantityButton); break;
			case "cucumber": return(.34 * this.quantityButton); break;
			case "corn": return(.55 * this.quantityButton); break;
			case "lettuce": return(1.12 * this.quantityButton); break;
			case "tomato": return(.32 * this.quantityButton); break;
        }
	}
};

// displays item selected in log
function itemSelect(item)
{
	document.getElementById("log").innerHTML += item;
	cashRegister.productButton = item;
}

// displays quantity selected in log
function quantitySelect(quantity)
{
	if (cashRegister.quantityButton == "")
	{
		document.getElementById("log").innerHTML += " * " + quantity;
	}
	else
	{
		document.getElementById("log").innerHTML += quantity;
	}
	cashRegister.quantityButton += quantity; 
	
}

// update the Log
function runningTotal()
{
	// update the log
	document.getElementById("log").innerHTML += " = $" + cashRegister.subtotal().toFixed(2) + "\n";

	// add to total
	cashRegister.scan();
	document.getElementById("total").value = "$" + cashRegister.total.toFixed(2);

	// reset variables
	cashRegister.quantityButton = "";
	cashRegister.productButton = "";

	var textArea = document.getElementById("log");
	textArea.scrollTop = 99999;
}

// clears any non-entered series of button entries
function clearLog()
{
	cashRegister.quantityButton = "";
	cashRegister.productButton = "";
	var log = document.getElementById("log").innerHTML.split("\n");
	document.getElementById("log").innerHTML = "";
	for (var i = 0; i < log.length-1; i++)
	{
		document.getElementById("log").innerHTML += log[i] + "\n";
	}
}

// clears the cashier for new sale
function newEntry()
{
	cashRegister.total = 0;
	cashRegister.quantityButton = "";
	cashRegister.productButton = "";
	document.getElementById("log").innerHTML = "";
	document.getElementById("total").value = "0.00";
}
	
